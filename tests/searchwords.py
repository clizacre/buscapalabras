import sys
from sortwords import sort, show

def search_word(word, sorted_list):
    try:
        position = sorted_list.index(word)
        return position
    except ValueError:
        raise Exception("La palabra no se encuentra en la lista ordenada.")

def main():
    # Verificar que se han pasado al menos dos argumentos en la línea de comandos
    if len(sys.argv) < 3:
        print("Uso: python3 searchwords.py <palabra_a_buscar> <lista_de_palabras>")
        sys.exit(1)

    word_to_search = sys.argv[1]
    words_list = sys.argv[2:]

    # Ordenar la lista de palabras utilizando la función sort del módulo sortwords
    sorted_list = sort(words_list)

    # Mostrar la lista ordenada utilizando la función show del módulo sortwords
    show(sorted_list)

    try:
        # Buscar la posición de la palabra y mostrarla
        position = search_word(word_to_search, sorted_list)
        print(position)
    except Exception as e:
        print(f"Error: {e}")
        sys.exit(1)

if __name__ == "__main__":
    main()

